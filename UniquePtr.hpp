#ifndef UNIQUE_PTR_HPP_
# define UNIQUE_PTR_HPP_

# include <iostream>

template <typename Type>
class UniquePtr
{
public:
  explicit UniquePtr(Type * ptr = 0)
    : _ptr(ptr)
  {

  }

  UniquePtr(const UniquePtr & o)
  {
    _ptr = new Type(*o);
  }

  UniquePtr & operator=(const UniquePtr & o)
  {
    if (&o == this)
      return *this;
    if (_ptr)
      delete _ptr;
    _ptr = new Type(*o);
    return *this;
  }

  UniquePtr & operator=(Type * ptr)
  {
    if (_ptr == ptr)
      return *this;
    if (_ptr)
      delete _ptr;
    _ptr = ptr;
    return *this;
  }

  virtual ~UniquePtr()
  {
    if (_ptr)
      delete _ptr;
  }

  Type * get()
  {
    return _ptr;
  }

  const Type * get() const
  {
    return _ptr;
  }

  Type * release()
  {
    Type * ret = _ptr;

    _ptr = 0;
    return ret;
  }

  void reset(Type * ptr = 0)
  {
    if (_ptr)
      delete _ptr;
    _ptr = ptr;
  }

  Type & operator*() const
  {
    return *_ptr;
  }

  Type * operator->() const
  {
    return _ptr;
  }

  Type & operator[](unsigned int offset)
  {
    return _ptr[offset];
  }

  bool operator==(const UniquePtr & o) const
  {
    return _ptr == o._ptr;
  }

  bool operator!=(const UniquePtr & o) const
  {
    return _ptr != o._ptr;
  }

  bool operator<(const UniquePtr & o) const
  {
    return _ptr < o._ptr;
  }

  bool operator<=(const UniquePtr & o) const
  {
    return _ptr <= o._ptr;
  }

  bool operator>(const UniquePtr & o) const
  {
    return _ptr > o._ptr;
  }

  bool operator>=(const UniquePtr & o) const
  {
    return _ptr >= o._ptr;
  }

  operator bool() const
  {
    return _ptr != 0;
  }

private:
  Type *	_ptr;
};

template <typename Type>
std::ostream & operator<<(std::ostream & stream, const UniquePtr<Type> & o)
{
  return stream << o.get();
}

#endif // !UNIQUE_PTR_HPP_
